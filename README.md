A collection of random courses, notes and coursework.

#Running stuff
- Install `python 3`
- Install `pipenv`
    - OSX: `brew install pipenv`
    - Other OS's: https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv
- The notes are in markdown whicis human readable and interpreted to a nice format
- The excersises are (usually) in jupyter notebooks.