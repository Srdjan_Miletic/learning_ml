 Hyperparameters = configuration setting to control training
 error :: abs(x - x')
 derivative of error = curve
 We want to do hill descent on the error curve until we reach a trough.
 That's a local error minimum
 = Gradient Descent
 
 learning rate = size of steps along gradient
 
 can compute gradient for random sample each time to minimize computation.
 
 
 ### Maths
 to find the gradient, differentiate with respect to all variables to find a vector
 
 question: how do we do multi-variate gradients?
 