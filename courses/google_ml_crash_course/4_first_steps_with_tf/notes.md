tensorflow = graph thingy than can be used for ML. Built on low level API's. Lots of high level stuff build on top of it.

example code:
```
import tensorflow as tf

# Set up a linear classifier.
classifier = tf.estimator.LinearClassifier(feature_columns)

# Train the model on some example data.
classifier.train(input_fn=train_input_fn, steps=2000)

# Use it to predict.
predictions = classifier.predict(input_fn=predict_input_fn)
```

### How does TF work
- executes operations via a distributed graph??

- protocall buffer = protobuf toolsgenerate classes which can manipulate the data in a text file.
- graph = network of nodes each representing one computation
    - Graph is directed. Edges represent inputs/outputs
    - Each node is an object with attrs
        - name = unique name
        - op = operations
        - ["nodeName1:2";"nodeName324:56"] = inputs. name and port
        - device = for distributed computing
        - attr: shittons of attributes.
