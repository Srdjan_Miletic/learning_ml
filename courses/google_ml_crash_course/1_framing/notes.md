- Supervised ml
- Label = target. Thing we're trying to predict. E.g: is an email spam
- Feature = data. e.g words in email
- Example = one piece of data. Labelled or unlabelled
- Model :: example => labeled example

- training = creating or learning the model
- inference = applying the model to unlabeled data

 - Regression vs Classification models
    - Regression = continuous data
    - Classification = discrete data

