line equation = y = wx + b
w = weight
b = bias

loss = error
loss abs(predictedY - actualY)

L2 loss = error^2
Mean Squared Error = 1/n * sum((predictedX - actualX) ^ 2)

Main Takaways:
- standard loss function is exponential. Bigger errors matter far more than multiple smaller errors